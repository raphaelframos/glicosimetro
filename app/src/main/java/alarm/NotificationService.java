package alarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import powellapps.com.glicosimetro.R;
import powellapps.com.glicosimetro.StartActivity;

/**
 * Created by raphael on 07/07/16.
 */
public class NotificationService extends Service{

    public static final String NOTIFICATION_INTENT = "powellapps.com.Push_Notification";
    private static final String FROM_NOTIFICATION_EXTRA_KEY = "powellapps.com.notification";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        criarNotificacao();
        cancelaAlarmes();
        sendBroadcast(new Intent(this, AlarmReceiver.class));
        stopSelf();
    }


    private void cancelaAlarmes() {
        PendingIntent pendingIntent = PendingIntent.getService(
                NotificationService.this, 0,
                new Intent(NOTIFICATION_INTENT), PendingIntent.FLAG_NO_CREATE);
        if (pendingIntent != null) {
            pendingIntent.cancel();
        }
    }

    private void criarNotificacao() {
        Intent it = new Intent(this, StartActivity.class);
        it.putExtra(FROM_NOTIFICATION_EXTRA_KEY, true);

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        Notification n  = new Notification.Builder(this)
                .setContentTitle("New mail from " + "test@gmail.com")
                .setContentText("Subject")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(null)
                .setAutoCancel(true)
                .addAction(R.mipmap.ic_launcher, "Call", null).build();


        notificationManager.notify(0, n);

    }
}
