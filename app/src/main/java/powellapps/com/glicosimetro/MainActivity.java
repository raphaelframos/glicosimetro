package powellapps.com.glicosimetro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.orm.query.Select;
import com.orm.util.NamingHelper;

import java.util.ArrayList;
import java.util.List;

import adapter.AdapterData;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.Data;

public class MainActivity extends AppCompatActivity {

    private AdapterData adapterData;
    @BindView(R.id.recycler_view_data) RecyclerView recyclerViewData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerViewData.setLayoutManager(llm);
        adapterData = new AdapterData(this, new ArrayList<Data>());
        recyclerViewData.setAdapter(adapterData);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()){
            case R.id.action_new:
                startActivity(new Intent(this,NewDataActivity.class));
                break;
        }
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        try{

            List<Data> dataList = Select.from(Data.class)
                    .orderBy(NamingHelper.toSQLNameDefault("date"))
                    .list();
            adapterData.update(dataList);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
