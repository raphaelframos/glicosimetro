package powellapps.com.glicosimetro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.orm.util.NamingHelper;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import model.Data;
import utils.DateTimeUtils;

public class NewDataActivity extends AppCompatActivity implements RadialTimePickerDialogFragment.OnTimeSetListener, CalendarDatePickerDialogFragment.OnDateSetListener {

    private static final String FRAG_TAG_TIME_PICKER = "timepicker";
    private static final String FRAG_TAG_DATE_PICKER = "datepicker";
    @BindView(R.id.edit_date)
    EditText editDate;
    @BindView(R.id.edit_hour)
    EditText editTime;
    @BindView(R.id.edit_value)
    EditText editValue;
    private Calendar calendar;
    private Data data;
    private boolean isEditing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_data);
        try {
            ButterKnife.bind(this);
            calendar = Calendar.getInstance();
            editDate.setOnFocusChangeListener(setDate);
            editTime.setOnFocusChangeListener(setTime);
            setTime();
            data = (Data) getIntent().getSerializableExtra("data");
            isEditing = data != null;
            if(isEditing){
                editValue.setText(data.getValue() + "");
                calendar.setTimeInMillis(data.getDate());
            }
            showDate();
            showTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTime() {
        int FIRST_HOUR = 10;
        int FIRST_MINUTE = 0;
        try{
            List<Data> dataList = Data.find(Data.class, NamingHelper.toSQLNameDefault("dateFormatted") + " = ?", new String[] {DateTimeUtils.formatDate(calendar)});
            if(dataList.size() > 0){
                FIRST_HOUR = 14;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(!isEditing){
            calendar.set(Calendar.HOUR_OF_DAY,FIRST_HOUR);
            calendar.set(Calendar.MINUTE, FIRST_MINUTE);
        }

    }

    private View.OnFocusChangeListener setTime = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus){

                RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(NewDataActivity.this)
                        .setStartTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))
                        .setDoneText("Salvar")
                        .setThemeCustom(R.style.MyCustomBetterPickersDialogs)
                        .setCancelText("Cancelar");
                rtpd.show(getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);
            }
        }
    };

    private View.OnFocusChangeListener setDate = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(hasFocus){
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(NewDataActivity.this)
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        .setThemeCustom(R.style.MyCustomBetterPickersDialogs)
                        .setPreselectedDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                        .setDoneText("Salvar")
                        .setCancelText("Cancelar");
                cdp.show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_new_data, menu);
        if(data != null){
            menu.findItem(R.id.action_delete).setVisible(true);
        }else{
            menu.findItem(R.id.action_delete).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {

            case R.id.action_delete:
                if(isEditing){
                    data.delete();
                    Toast.makeText(getApplicationContext(),"Medição removida!", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;

            case R.id.action_save:
                String value = editValue.getText().toString();
                if(value.isEmpty()){
                    editValue.setError("Campo em branco!");
                }else{
                    if(isEditing) {
                        data = Data.findById(Data.class,data.getId());
                    }else{
                        data = new Data();
                    }
                    data.setValue(Integer.valueOf(value));
                    data.setDate(calendar.getTimeInMillis());
                    data.setDateFormatted(DateTimeUtils.formatDate(calendar));
                    data.save();
                    Toast.makeText(getApplicationContext(),"Medição realizada com sucesso.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
        return true;
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        showTime();
    }

    private void showTime() {
        editTime.setText(DateTimeUtils.formatTime(calendar));
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        showDate();
    }

    private void showDate() {
        editDate.setText(DateTimeUtils.formatDate(calendar));
        setTime();
    }
}
