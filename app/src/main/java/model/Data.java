package model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by raphael on 06/07/16.
 */
public class Data extends SugarRecord implements Serializable {

    private Long date;
    private int value;
    private String dateFormatted;
    private Long id;

    public Data(){}

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getDateFormatted() {
        return dateFormatted;
    }

    public void setDateFormatted(String dateFormatted) {
        this.dateFormatted = dateFormatted;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
