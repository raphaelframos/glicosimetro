package adapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import model.Data;
import powellapps.com.glicosimetro.NewDataActivity;
import powellapps.com.glicosimetro.R;
import utils.DateTimeUtils;

/**
 * Created by raphael on 06/07/16.
 */
public class AdapterData extends RecyclerView.Adapter<AdapterData.DataViewHolder> {

    private List<Data> dataList;
    private AppCompatActivity activity;

    public AdapterData(AppCompatActivity mainActivity, List<Data> dataList){
        this.dataList = dataList;
        this.activity = mainActivity;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_data, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        final Data data = dataList.get(position);
        if(data != null){
            holder.textDate.setText(DateTimeUtils.formatDate(new Date(data.getDate())));
            holder.textTime.setText(DateTimeUtils.formatTime(new Date(data.getDate())));
            holder.textValue.setText("" + data.getValue());
            holder.imageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(activity, NewDataActivity.class);
                    it.putExtra("data", data);
                    activity.startActivity(it);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void update(List<Data> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    class DataViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.text_view_date)
        TextView textDate;

        @BindView(R.id.text_view_time)
        TextView textTime;

        @BindView(R.id.image_edit)
        ImageView imageEdit;

        @BindView(R.id.text_view_value)
        TextView textValue;

        public DataViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
